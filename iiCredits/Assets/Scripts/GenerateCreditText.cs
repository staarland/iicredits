﻿//---------------------------------------------------------
/// Generate Credit Text
/// 
/// Script that loads in the names files and generates a string based on those names
/// 
/// Copyright 2015 Insert Imagination
/// 
/// @author S Taarland
/// 
/// @date 24th January 2015
//---------------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GenerateCreditText : MonoBehaviour 
{
	private string[] firstNames;
	private string[] nickNames;
	private string[] lastNames;
	private string[] titleNames;
	private string[] jobNames;
	private string[] startingText;

	//---------------------------------------------------------
	/// Start Function to load in Text Resources
	///
	/// @author S Taarland
	//---------------------------------------------------------
	void Start () 
	{
		startingText = Resources.Load<TextAsset>("Text/StartText").text.Split('\n');
		firstNames = Resources.Load<TextAsset>("Text/FirstNames").text.Split('\n');
		nickNames = Resources.Load<TextAsset>("Text/Nicknames").text.Split('\n');
		lastNames = Resources.Load<TextAsset>("Text/Surnames").text.Split('\n');
		titleNames = Resources.Load<TextAsset>("Text/Title").text.Split('\n');
		jobNames = Resources.Load<TextAsset>("Text/Job").text.Split('\n');
	}
	//---------------------------------------------------------
	/// Get Credit Text
	///
	/// @author S Taarland
	/// 
	/// @param key for array
	/// 
	/// @return Credit Text
	//---------------------------------------------------------
	public string GetCreditText(int in_key = 99999)
	{
		string creditText;
		if(in_key != 99999)
		{
			creditText = GetFirstNameText(in_key)+GetNicknameText(in_key)+GetLastNameText(in_key)+GetTitleText(in_key)+GetJobText(in_key);
		}
		else
		{
			creditText = GetFirstNameText(Random.Range(0,firstNames.Length))
							+GetNicknameText(Random.Range(0,nickNames.Length))
							+GetLastNameText(Random.Range(0,lastNames.Length))
							+GetTitleText(Random.Range(0,titleNames.Length))
							+GetJobText(Random.Range(0,jobNames.Length));
		}
		return creditText;
	}
	//---------------------------------------------------------
	/// Get firstName Text
	///
	/// @author S Taarland
	/// 
	/// @param key for array
	/// 
	/// @return first name string
	//---------------------------------------------------------
	string GetFirstNameText(int in_key)
	{
		return firstNames[in_key] +" ";
	}
	//---------------------------------------------------------
	/// Get lastName Text
	///
	/// @author S Taarland
	/// 
	/// @param key for array
	/// 
	/// @return last name string
	//---------------------------------------------------------
	string GetLastNameText(int in_key)
	{
		return lastNames[in_key] +": ";
	}
	//---------------------------------------------------------
	/// Get nickname Text
	///
	/// @author S Taarland
	/// 
	/// @param key for array
	/// 
	/// @return nickname name string
	//---------------------------------------------------------
	string GetNicknameText(int in_key)
	{
		if(Random.Range(0,100) > 70)
		{
			return "";
		}
		return nickNames[in_key] +" ";
	}
	//---------------------------------------------------------
	/// Get titleName Text
	///
	/// @author S Taarland
	/// 
	/// @param key for array
	/// 
	/// @return title name string
	//---------------------------------------------------------
	string GetTitleText(int in_key)
	{
		return titleNames[in_key] +" ";
	}
	//---------------------------------------------------------
	/// Get JobName Text
	///
	/// @author S Taarland
	/// 
	/// @param key for array
	/// 
	/// @return job name string
	//---------------------------------------------------------
	string GetJobText(int in_key)
	{
		return jobNames[in_key] +" ";
	}
	//---------------------------------------------------------
	/// Get Size of starting Text
	///
	/// @author S Taarland
	///
	/// @return starting text size
	//---------------------------------------------------------
	public int GetSizeOfStartingText()
	{
		return startingText.Length;
	}
	//---------------------------------------------------------
	/// Get Size of starting Text
	///
	/// @author S Taarland
	///
	/// @return starting text size
	//---------------------------------------------------------
	public string GetStartingString(int in_key)
	{
		return startingText[in_key];
	}
	
}
