//---------------------------------------------------------
/// Create Credit
/// 
/// Script that creates a credit object and sets UI
/// 
/// Copyright 2015 Insert Imagination
/// 
/// @author S Taarland
/// 
/// @date 24th January 2015
//---------------------------------------------------------
using UnityEngine;
using System.Collections;

public class CreateCredit : MonoBehaviour 
{
	public GameObject creditPrefab;

	public float ySpacing = 100.0f;
	private float randomTimer;
	public float nextRandomTimer;
	//---------------------------------------------------------
	/// Start Function to create Initial Credits
	///
	/// @author S Taarland
	//---------------------------------------------------------
	void Start () 
	{
		int noCredits = gameObject.GetComponent<GenerateCreditText>().GetSizeOfStartingText();
		CreatingStartingCredits();
	}
	//---------------------------------------------------------
	/// Update
	///
	/// @author S Taarland
	//---------------------------------------------------------
	void Update () 
	{
		randomTimer -= Time.deltaTime;
		if(randomTimer <= 0.0f)
		{
			randomTimer = Random.Range(1, nextRandomTimer);
			CreateNewCredit();
		}
	}
	//---------------------------------------------------------
	/// Create a new random credit
	///
	/// @author S Taarland
	//---------------------------------------------------------
	void CreateNewCredit()
	{
		GenerateCreditText textGenerator = gameObject.GetComponent<GenerateCreditText>() as GenerateCreditText;

		GameObject newCredit = Instantiate(creditPrefab) as GameObject;
		newCredit.GetComponent<CreditController>().SetCreditText(textGenerator.GetCreditText());
		newCredit.transform.parent = gameObject.transform;
		
		var position = newCredit.GetComponent<RectTransform>().transform.position;
		position.x = Screen.width/2;
		position.y = ySpacing;
		newCredit.GetComponent<RectTransform>().transform.position = position;
	}
	//---------------------------------------------------------
	/// Creating Starting Credits
	///
	/// @author S Taarland
	//---------------------------------------------------------
	void CreatingStartingCredits()
	{
		GenerateCreditText textGenerator = gameObject.GetComponent<GenerateCreditText>() as GenerateCreditText;
		int noCredits = gameObject.GetComponent<GenerateCreditText>().GetSizeOfStartingText();
		int i = 0;
		for(i = 0; i < noCredits; ++i)
		{
			GameObject newCredit = Instantiate(creditPrefab) as GameObject;
			newCredit.GetComponent<CreditController>().SetCreditText(textGenerator.GetStartingString(i));
			newCredit.transform.parent = gameObject.transform;

			var position = newCredit.GetComponent<RectTransform>().transform.position;
			position.x = Screen.width/2;
			position.y = i * ySpacing;
			newCredit.GetComponent<RectTransform>().transform.position = position;
		}
		int w = i + 6;
		for(i = noCredits; i < w; ++i)
		{
			GameObject newCredit = Instantiate(creditPrefab) as GameObject;
			newCredit.GetComponent<CreditController>().SetCreditText(textGenerator.GetCreditText(i-6));
			newCredit.transform.parent = gameObject.transform;
			
			var position = newCredit.GetComponent<RectTransform>().transform.position;
			position.x = Screen.width/2;
			position.y = i * ySpacing;
			newCredit.GetComponent<RectTransform>().transform.position = position;
		}

		randomTimer = ((i + 1) * nextRandomTimer);
		int x = 0;
	}
}
