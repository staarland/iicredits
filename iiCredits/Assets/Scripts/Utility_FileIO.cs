﻿using System.Collections;

public class Utility_FileIO {

	private string FILE_NAME;
	System.IO.StreamWriter Writer;

	
	public void Start (string name) {
		FILE_NAME = @"OutputLogs\"+name+System.DateTime.Now.ToString("dd_MM_yyyy")+"Output.txt";
		if(System.IO.File.Exists(FILE_NAME)){
			Writer = new System.IO.StreamWriter(FILE_NAME,true);
			//Writer.
			Writer.WriteLine("Updating File: "+System.DateTime.Now.ToString("hh:mm:ss"));
		}
		else{
			Writer = new System.IO.StreamWriter(FILE_NAME);
			Writer.WriteLine("Starting New File at: "+System.DateTime.Now.ToString("hh:mm:ss"));

		}
	}//end of Start function
	

	public void WriteNewLine (string newLine) {
		Writer.WriteLine(System.DateTime.Now.ToString("hh:mm:ss")+newLine);
	}

	public void Remove(){

		Writer.Close();
	}
}
