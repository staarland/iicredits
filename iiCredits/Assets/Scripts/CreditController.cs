﻿///--------------------------------------------------------
/// Credit Controller
/// 
/// Controls a credit object
/// 
/// Copyright 2015 Insert Imagination
/// 
/// @author S Taarland
/// 
/// @date 24th January 2015
//---------------------------------------------------------
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreditController : MonoBehaviour {

	private string creditText;
	public float velocity;
	///--------------------------------------------------------
	/// Start
	/// 
	/// @author S Taarland
	//---------------------------------------------------------
	void Start () 
	{

	}
	///--------------------------------------------------------
	/// Update
	/// 
	/// @author S Taarland
	//---------------------------------------------------------
	void Update () 
	{
		var position = gameObject.transform.position;
		position.y += Time.deltaTime * velocity;
		gameObject.transform.position = position;

		//TODO: cleanup object if off screen
	}
	///--------------------------------------------------------
	/// Set Credit Text
	/// 
	/// @author S Taarland
	/// 
	/// @param text to be set to the ui component
	//---------------------------------------------------------
	public void SetCreditText(string in_text)
	{
		creditText = in_text;

		GetComponent<Text>().text = creditText;
		//TODO: resize the transform to fit text
	}
	///--------------------------------------------------------
	/// On Trigger Enter
	/// 
	/// @author S Taarland
	/// 
	/// @param collision object
	//---------------------------------------------------------
	void OnTriggerEnter(Collider in_collision)
	{
		if(in_collision.transform.tag == "EndZone")
		{
			Destroy(gameObject);
		}
		if(in_collision.transform.tag == "Cursor")
		{
			//TODO:
			//Destroy(in_collision);
		}
		
	}
}
